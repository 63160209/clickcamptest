// const divApp = document.getElementById('app')
// divApp.innerText = 'Hello Javascript! Again!'

const inputNumber = document.getElementById('inputNumber')
const plusButton = document.getElementById('plusButton')
const minusButton = document.getElementById('minusButton')
const multiplyButton = document.getElementById('multiplyButton')
const divideButton = document.getElementById('divideButton')
const equalButton = document.getElementById('equalButton')
const notEqualButton = document.getElementById('notEqualButton')
const greaterThanButton = document.getElementById('greaterThanButton')
const greaterThanOrEqualButton = document.getElementById('greaterThanOrEqualButton')
const lessThanButton = document.getElementById('lessThanButton')
const lessThanOrEqualButton = document.getElementById('lessThanOrEqualButton')
const output = document.getElementById('output')

plusButton.addEventListener('click', function (){
    const value = inputNumber.value
    const result = (+value) + 2
    output.innerText = result
})

minusButton.addEventListener('click', function (){
    const value = inputNumber.value
    const result = value - 2
    output.innerText = result
})

multiplyButton.addEventListener('click', function (){
    const value = inputNumber.value
    const result = value * 2
    output.innerText = result
})

divideButton.addEventListener('click', function (){
    const value = inputNumber.value
    const result = value / 2
    output.innerText = result + ''
})

equalButton.addEventListener('click', function (){
    const value = +inputNumber.value
    const result = value === 10
    output.innerText = result + ''
})

notEqualButton.addEventListener('click', function (){
    const value = +inputNumber.value
    const result = value !== 10
    output.innerText = result + ''
})

greaterThanButton.addEventListener('click', function (){
    const value = +inputNumber.value
    const result = value > 10
    output.innerText = result + ''
})

greaterThanOrEqualButton.addEventListener('click', function (){
    const value = +inputNumber.value
    const result = value >= 10
    output.innerText = result + ''
})

lessThanButton.addEventListener('click', function (){
    const value = +inputNumber.value
    const result = value < 10
    output.innerText = result + ''
})

lessThanOrEqualButton.addEventListener('click', function (){
    const value = +inputNumber.value
    const result = value <= 10
    output.innerText = result + ''
})