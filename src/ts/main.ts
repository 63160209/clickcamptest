// const divApp: HTMLDivElement = document.getElementById('app') as HTMLDivElement
// divApp.innerText = 'Hello TypeScript! Na!'

const inputNumber = document.getElementById('inputNumber') as HTMLInputElement
const plusButton = document.getElementById('plusButton') as HTMLButtonElement
const minusButton = document.getElementById('minusButton') as HTMLButtonElement
const multiplyButton = document.getElementById('multiplyButton') as HTMLButtonElement
const divideButton = document.getElementById('divideButton') as HTMLButtonElement
const equalButton = document.getElementById('equalButton') as HTMLButtonElement
const notEqualButton = document.getElementById('notEqualButton') as HTMLButtonElement
const greaterThanButton = document.getElementById('greaterThanButton') as HTMLButtonElement
const greaterThanOrEqualButton = document.getElementById('greaterThanOrEqualButton') as HTMLButtonElement
const lessThanButton = document.getElementById('lessThanButton') as HTMLButtonElement
const lessThanOrEqualButton = document.getElementById('lessThanOrEqualButton') as HTMLButtonElement
const output = document.getElementById('output') as HTMLParagraphElement

plusButton.addEventListener('click', function () {
    const value: number = +inputNumber.value
    const result = value + 2
    output.innerText = result + ''
})

minusButton.addEventListener('click', function () {
    const value: number = +inputNumber.value
    const result = value - 2
    output.innerText = result + ''
})

multiplyButton.addEventListener('click', function () {
    const value: number = +inputNumber.value
    const result = value * 2
    output.innerText = result + ''
})

divideButton.addEventListener('click', function () {
    const value: number = +inputNumber.value
    const result = value / 2
    output.innerText = result + ''
})

equalButton.addEventListener('click', function () {
    const value: number = +inputNumber.value
    const result = value === 10
    output.innerText = result + ''
})

notEqualButton.addEventListener('click', function () {
    const value: number = +inputNumber.value
    const result = value !== 10
    output.innerText = result + ''
})

greaterThanButton.addEventListener('click', function () {
    const value: number = +inputNumber.value
    const result = value > 10
    output.innerText = result + ''
})

greaterThanOrEqualButton.addEventListener('click', function () {
    const value: number = +inputNumber.value
    const result = value >= 10
    output.innerText = result + ''
})

lessThanButton.addEventListener('click', function () {
    const value: number = +inputNumber.value
    const result = value < 10
    output.innerText = result + ''
})

lessThanOrEqualButton.addEventListener('click', function () {
    const value: number = +inputNumber.value
    const result = value <= 10
    output.innerText = result + ''
})

const person: Person = {
    name: 'Jame Done',
    age: 22,
    address: {
        street: '147 Main Street',
        city: 'San Francriga',
        country: 'Thailand'
    },
    hobbies: ['coding', 'reading', 'music', 'sleep']
}

interface Address {
    street: string
    city: string
    country: string
}

interface Person {
    name: string
    age: number
    address: Address
    hobbies: string[]
}

// console.log(person.address.city)

// function log(message: string): void {
//     return 
// }
// function add(number1: number, number2: number):number {
//     return number1 + number2
// }

function DisplayDetail(person: Person): string {
    return 'name = ' + person.name + '; age = ' 
    + person.age + ': address = ' 
    + person.address.street 
    + person.address.city + ' ' 
    + person.address.country + '; hobbies= ' 
    + person.hobbies.join(', ')
}

// console.log(DisplayDetail(person))
// console.log(DisplayDetail({
//     name: 'P K',
//     age: 22,
//     address: {
//         street: '111 Main Street',
//         city: 'Samut',
//         country: 'Thailand'
//     },
//     hobbies: ['sleep']
// }))

const address: Address[] = [
    { street: '224', city: '236', country: '248'},
    { street: '326', city: '339', country: '3412'},
    { street: '428', city: '4312', country: '4416'},
]

console.log(address)

const n: number = 10
const s: string = "T_T"
const b: boolean = true
const array: number[] = [1, 2, 3]

export { }