interface Question {
  question: string;
  choices: string[];
  correctAnswer: number;
}

const questions: Question[] = [
  {
    question:
      "What is the output of the following code? console.log(typeof null);",
    choices: ['"object"', '"null"', '"undefined"', '"boolean"'],
    correctAnswer: 0,
  },
  {
    question:
      "Which method is used to add one or more elements to the end of an array?",
    choices: ["push()", "join()", "slice()", "concat()"],
    correctAnswer: 0,
  },
  {
    question: 'What is the result of the following expression? 3 + 2 + "7"',
    choices: ['"327"', '"12"', '"57"', '"NaN"'],
    correctAnswer: 2,
  },
  {
    question:
      'What is the purpose of the "use strict" directive in JavaScript?',
    choices: [
      "Enforce stricter type checking",
      "Enable the use of modern syntax",
      "Enable strict mode for improved error handling",
      "Disable certain features for better performance",
    ],
    correctAnswer: 2,
  },
  {
    question:
      'What is the scope of a variable declared with the "let" keyword?',
    choices: ["Function scope", "Global scope", "Block scope", "Module scope"],
    correctAnswer: 2,
  },
  {
    question:
      "Which higher-order function is used to transform elements of an array into a single value?",
    choices: ["map()", "filter()", "reduce()", "forEach()"],
    correctAnswer: 2,
  },
  {
    question: 'What does the "=== " operator in JavaScript check for?',
    choices: [
      "Equality of values",
      "Equality of values and types",
      "Inequality of values",
      "Reference equality",
    ],
    correctAnswer: 1,
  },
  {
    question: 'What is the purpose of the "this" keyword in JavaScript?',
    choices: [
      "Refer to the current function",
      "Refer to the parent function",
      "Refer to the global object",
      "Refer to the object that owns the current code",
    ],
    correctAnswer: 3,
  },
  {
    question: 'What does the "NaN" value represent in JavaScript?',
    choices: ["Not a Number", "Null", "Negative Number", "Not Applicable"],
    correctAnswer: 0,
  },
  {
    question: "Which method is used to remove the last element from an array?",
    choices: ["pop()", "shift()", "slice()", "splice()"],
    correctAnswer: 0,
  },
];

const ex2 = document.getElementById("exercise2") as HTMLDivElement;
const curSco = document.createElement("div");
const div = document.createElement("div");
curSco.innerText = "Current Score: 0/10";
let scr = 0;
ex2.appendChild(curSco);

for (const q of questions) {
  const frm = document.createElement("form");
  const b = document.createElement("p");
  ex2.appendChild(frm);
  const div = document.createElement("div");
  const ques = document.createElement("p");
  frm.appendChild(div);
  ques.innerText = q.question;
  div.appendChild(ques);
  const radios: HTMLInputElement[] = []; // สร้างอาร์เรย์เพื่อเก็บ radio button
  for (let i = 0; i < q.choices.length; i++) {
    const div2 = document.createElement("div");
    div.appendChild(div2);
    const rad = document.createElement("input");
    rad.type = "radio";
    rad.name = "cText";
    rad.value = i.toString(); // ตั้งค่า value ของ radio button เป็นตำแหน่งตัวเลือก
    div2.appendChild(rad);
    const choi = document.createElement("label");
    choi.innerText = q.choices[i];
    div2.appendChild(choi);
    radios.push(rad); // เพิ่ม radio button เข้าสู่อาร์เรย์
  }
  const submit = document.createElement("button");
  submit.innerText = "Submit";
  div.appendChild(submit);
  div.appendChild(b)

  submit.addEventListener("click", function (event) {
    event.preventDefault(); // ป้องกันการรีเฟรชหรือส่งฟอร์ม
  
    const selectedRadio = radios.find((radio) => radio.checked);
    if (selectedRadio) {
      submit.disabled = true;
      radios.forEach((radio) => {
        radio.disabled = true; // ปิดการเลือก radio button
      });
  
      const userAnswer = parseInt(selectedRadio.value);
      if (userAnswer === q.correctAnswer) {
        b.innerText = 'Correct!'
        scr += 1;
        curSco.innerText = "Current Score: " + scr + "/10";
        // เพิ่มคะแนน
      } else {
        b.innerText = 'Incorrect!'
      }
    } else {
      alert("Please choose an answer first!");
    }
  });
}

